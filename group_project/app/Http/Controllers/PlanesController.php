<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\planes\Plane;

class PlanesController extends Controller
{
    public function index()
    {
        $planes = Plane::all();
        return view('planes.index', compact('planes'));
    }
    public function show($id){
        $plane = Plane::where('id',$id)->firstOrFail();
        return view('planes.show',compact('plane'));
    }
}
