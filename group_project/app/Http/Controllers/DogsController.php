<?php
/**
 * Created by PhpStorm.
 * User: Krita
 * Date: 2019-10-17
 * Time: 2:00 PM
 */

namespace App\Http\Controllers;


use App\Dog;

class DogsController
{

    public function index()
    {
        $dogs = Dog::all();
        return view('dogs.index', compact('dogs'));
    }

    public function show($dog_id)
    {
        $dog = Dog::findOrFail($dog_id);
        return view('dogs.show', compact('dog'));
    }

}