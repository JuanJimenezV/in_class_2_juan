<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(seed_dogs_table::class);
        $this->call(seed_cars_table::class);
        $this->call(seed_planes_table::class);
    }
}
