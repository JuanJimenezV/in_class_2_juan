<?php

use Illuminate\Database\Seeder;

class seed_dogs_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dogs')->insert([
            'name' => 'Fido',
            'description' => 'Fido is a black and white puppy',
            'image' => 'fido.jpg'
        ]);
        DB::table('dogs')->insert([
            'name' => 'Oreo',
            'description' => 'Oreo is a playful puppy',
            'image' => 'oreo.jpg'
        ]);
        DB::table('dogs')->insert([
            'name' => 'Hutch',
            'description' => 'Hutch enjoys digging dogs',
            'image' => 'hutch.jpg'
        ]);
        DB::table('dogs')->insert([
            'name' => 'Murice',
            'description' => 'Murice loves to play fetch',
            'image' => 'murice.jpg'
        ]);
        DB::table('dogs')->insert([
            'name' => 'Happy',
            'description' => 'Happy is such a happy guy',
            'image' => 'happy.jpg'
        ]);
    }
}
