<?php

use Illuminate\Database\Seeder;


class seed_planes_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('planes')->insert([
            'name' => 'Boeing 777',
            'description' => 'Air Canada',
            'image' => 'air-canada.jpg'
        ]);
        DB::table('planes')->insert([
            'name' => 'Boeing 787',
            'description' => 'Southwest',
            'image' => 'southwest.jpg'
        ]);

        DB::table('planes')->insert([
            'name' => 'Boeing 767',
            'description' => 'China Air',
            'image' => 'chona-air.jpg'
        ]);

        DB::table('planes')->insert([
            'name' => 'Boeing 787',
            'description' => 'West Jet',
            'image' => 'west-jet.jpg'
        ]);

        DB::table('planes')->insert([
            'name' => 'Boeing 777',
            'description' => 'Air France',
            'image' => 'air-france.jpg'
        ]);

    }
}
