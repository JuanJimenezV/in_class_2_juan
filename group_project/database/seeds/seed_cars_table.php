<?php

use Illuminate\Database\Seeder;

class seed_cars_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cars')->insert([
        	'name' => 'Maruti',
        	'description' => 'this is my car',
        	'image' => 'maruti.jpg'
        ]);

        DB::table('cars')->insert([
        	'name' => 'BMW',
        	'description' => 'this is my car',
        	'image' => 'bmw.jpg'
        ]);

        DB::table('cars')->insert([
        	'name' => 'Audi',
        	'description' => 'this is my car',
        	'image' => 'audi.jpg'
        ]);

        DB::table('cars')->insert([
        	'name' => 'one',
        	'description' => 'this is my car',
        	'image' => 'one.jpg'
        ]);

        DB::table('cars')->insert([
        	'name' => 'two',
        	'description' => 'this is my car',
        	'image' => 'two.jpg'
        ]);
    }
}
