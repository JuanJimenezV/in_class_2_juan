<?php
/**
 * Created by PhpStorm.
 * User: Krita
 * Date: 2019-10-17
 * Time: 1:58 PM
 */

Route::get('dogs', 'DogsController@index');
Route::get('dog/{dog_id}', 'DogsController@show');