<?php
/**
 * Created by PhpStorm.
 * User: Krita
 * Date: 2019-10-17
 * Time: 1:57 PM
 */

Route::get('/plane', 'PlanesController@index');
Route::get('/plane/{id}', 'PlanesController@show');
