@extends('layout')

@section('content')


    <h1>
        Dog List
    </h1>

    <table class="table table-bordered table-hover">

        <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Image</th>
            <th>Created At</th>
            <th>Action</th>
        </tr>
        </thead>

        <tbody>
        @foreach($planes as $plane)

            <tr>
                <td>{{ $plane->name }}</td>
                <td>{{ $plane->description }}</td>
                <td>{{ $plane->image }}</td>
                <td>{{ $plane->created_at }}</td>
                <td>
                    <a href="/plane/{{ $plane->id }}" class="btn
                            btn-primary btn-xs"><i
                            class="fa fa-eye"></i>View</a>
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

@endsection
