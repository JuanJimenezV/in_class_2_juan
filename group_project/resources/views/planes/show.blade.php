@extends('layout')
@section('content')
    <!-- Post Content Column -->
    <div class="col-lg-8">

        <!-- Title -->
        <h1 class="mt-4">{{$plane->name}}</h1>

        <!-- Author -->
        <p class="lead">
            by
            <a href="#">{{$plane->description}}</a>
        </p>

        <hr>

        <!-- Date/Time -->
        <p>{{$plane->description}}</p>

        <hr>

        <!-- Preview Image -->
        <img class="img-fluid rounded" src="/planes/{{$plane->image}}" alt="" style="width: 100px; height: 100px;">

        <hr>

        <!-- Post Content -->
        <p>{{$plane->description}}</p>




    </div>
@endsection
