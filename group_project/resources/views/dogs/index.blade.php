@extends('layout')

@section('content')

    <h1 class="mt-4">
        Dog List
    </h1>

    <table class="table table-bordered table-hover">

        <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Image</th>
            <th>Created At</th>
            <th>Action</th>
        </tr>
        </thead>

        <tbody>
        @foreach($dogs as $dog)

            <tr>
                <td>{{ $dog->name }}</td>
                <td>{{ $dog->description }}</td>
                <td>{{ $dog->image }}</td>
                <td>{{ $dog->created_at }}</td>
                <td>
                    <a href="dog/{{ $dog->id }}" class="btn
                            btn-primary btn-xs"><i
                                class="fa fa-eye"></i>View</a>
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

@endsection