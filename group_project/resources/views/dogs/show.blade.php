@extends('layout')

@section('content')

    <h1 class="mt-4">
        {{ $dog->name }}
    </h1>

    <p class="lead">
        Posted On
        <strong>{{ $dog->created_at }}</strong>
    </p>

    <hr />

    <img class="img-fluid rounded" src="/images/{{ $dog->image }}" alt="">
    <hr>

    <div>
        {!! $dog->description !!}
    </div>

@endsection