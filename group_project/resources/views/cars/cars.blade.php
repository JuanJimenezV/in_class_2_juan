@extends('layout')

@section('content')


<h1>Cars List</h1>

<table class="table">
	<tr>
		<td>Name</td>
		<td>Description</td>
		<td>Image</td>
		<td>Action</td>
	</tr>

	@foreach($cars as $car)
	<tr>
		<td>{{$car->name}}</td>
		<td>{{$car->description}}</td>
		<td><img src="/cars/{{$car->image}}" /></td>
		<td><a href="car/{{ $car->id }}" class="btn
                    btn-primary">
                View
            </a>
        </td>
	</tr>
	@endforeach
</table>


@endsection
